var Engine        = require('nedb');
var path          = require('path');
var moviesDb      = new Engine({filename: path.join('data', 'movies.db'), autoload: true});
var seriesDb      = new Engine({filename: path.join('data', 'series.db'), autoload: true});
var settingsDb    = new Engine({filename: path.join('data', 'settings.db'), autoload: true});
var directoriesDb = new Engine({filename: path.join('data', 'directories.db'), autoload: true});

module.exports = {
	movies: moviesDb,
	series: seriesDb,
	settings: settingsDb,
	directories: directoriesDb
};