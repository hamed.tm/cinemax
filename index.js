var app   = require('./app');
var debug = require('debug')('cinemax:server');
var http  = require('http');

// Get port from environment
var port = normalizePort(process.env.PORT || 9000);
app.set('port', port);

// Create server
var server = http.createServer(app);

// Listen on port
server.listen(port);
server.on('listening', onListening);

// Normalize a port into a number, string, or false.
function normalizePort(val) {
	var port = parseInt(val, 10);

	if (isNaN(port)) {
		// named pipe
		return val;
	}

	if (port >= 0) {
		// port number
		return port;
	}

	return false;
}

function onListening() {
	var address = server.address();
	var bind    = typeof address === 'string'
		? 'pipe ' + address
		: 'port ' + address.port;
	debug('CinemaX started on ' + bind);
}