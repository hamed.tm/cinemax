var express = require('express');
var db      = require('../db/db');

var router = express.Router();

router.get('/', function (req, res) {
	db.movies.find({}, function (err, movies) {
		res.render('movies', {movies: movies});
	})
});

module.exports = router;