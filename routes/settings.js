var express = require('express');
var fs      = require('fs');
var path    = require('path');
var db      = require('../db/db');
var seeker  = require('../seeker');

var router = express.Router();

router.get('/', function (req, res) {
	db.directories.find({}, function (err, directories) {
		res.render('settings', {directories: directories});
	});
});

router.post('/directories', function (req, res) {
	if (req.body.directory) {
		var directory = path.normalize(req.body.directory.toLowerCase());
		try {
			fs.accessSync(directory, fs.F_OK);
			db.directories.findOne({path: directory}, function (errorExist, exist) {
				if (exist) {
					res.sendStatus(409);
				} else {
					db.directories.insert({
						path: directory,
						include: req.body.include ? true : false
					}, function (errInsert, saved) {
						if (!errInsert) {
							res.render('directory/item', {directory: saved});
						} else {
							res.sendStatus(500);
						}
					});
				}
			});
		} catch (x) {
			res.sendStatus(404);
		}
	} else {
		res.sendStatus(411);
	}
});

router.get('/directories/:id/refresh', function (req, res) {
	db.directories.findOne({_id: req.params.id}, function (err, directory) {
		if (directory) {
			seeker(directory, function () {
				res.send('ok');
			});
		}
	});
});

module.exports = router;