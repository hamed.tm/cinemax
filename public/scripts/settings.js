var errorMessage = $('.directory-error-message');
$('#addDirectoryForm').on('submit', function (e) {
	e.preventDefault();
	errorMessage.html('');
	$.ajax({
		url: '/settings/directories',
		method: 'POST',
		data: $(this).serialize()
	}).success(function (data) {
		$('#addDirectoryModal').modal('hide');
		$('.no-directory').remove();
		$('.directories-container').append(data);
	}).error(function (data) {
		if (data.status == 409) {
			errorMessage.append($('<div class="alert alert-danger"></div>').html('Item Exist.'));
		}
		if (data.status == 404) {
			errorMessage.append($('<div class="alert alert-danger"></div>').html('Path Not Found.'));
		}
		if (data.status == 411) {
			errorMessage.append($('<div class="alert alert-danger"></div>').html('Path Required.'));
		}
		if (data.status == 409) {
			errorMessage.append($('<div class="alert alert-danger"></div>').html('Save Error.'));
		}
	})
});