var fs              = require('fs');
var _               = require('lodash');
var async           = require('async');
var debug           = require('debug')('cinemax:fs');
var path            = require('path');
var url             = require('url');
var TVDB            = require('node-tvdb');
var parseName       = require('parse-torrent-name');
var info            = require('movie-info');
var request         = require('request');
var db              = require('./db/db');
var movieGenres     = require('./config/movie-genres');
var tvGenres        = require('./config/tv-genres');
var validExtentions = require('./config/valid-extensions');
var tv              = new TVDB('6BB64A9E220D68A8');

var updateGenre      = function (data, res, callback) {
	if (data.type == 'movie') {
		data.genres = _.intersectionWith(movieGenres, res.genre_ids, function (a, b) {
			return a.id == b;
		});
		callback(data);
	}
	if (data.type == 'tv') {
		data.genres = _.intersectionWith(tvGenres, res.genre_ids, function (a, b) {
			return a.id == b;
		});
		callback(data);
	}
};
var updateMovieData  = function (savedData, finishCallback) {
	var year = savedData.year || '';
	info(savedData.title, year, function (err, res) {
		if (res) {
			updateGenre(savedData, res, function (data) {
				data.overview     = res.overview;
				data.release_date = res.release_date;
				data.rate         = res.vote_average;
				db.movies.update({_id: data._id}, data, function () {
					async.parallel([
						function (callbackPoster) {
							request('http://image.tmdb.org/t/p/w500' + res.poster_path)
								.pipe(fs.createWriteStream('data/' + data._id + '_poster.jpg'))
								.on('close', function () {
										callbackPoster(null);
									}
								);
						},
						function (callbackPoster) {
							request('http://image.tmdb.org/t/p/w780' + res.backdrop_path)
								.pipe(fs.createWriteStream('data/' + data._id + '_backdrop.jpg'))
								.on('close', function () {
										callbackPoster(null);
									}
								);
						}
					], function () {
						finishCallback();
					});
				});
			});
		} else {
			finishCallback();
		}
	});
};
var updateSerieData  = function (savedData, finishCallback) {
	tv.getSeriesByName(savedData.title, function (err, res) {
		data.overview     = res[0].overview;
		console.log(res[0]);
		tv.getBanners(res[0].seriesid, function (errorBanner, banners) {
			var enBanners           = _.filter(banners, {Language: 'en'});
			var ratedBanners        = _.filter(enBanners, function (o) {
				return o.Rating != null;
			});
			var posters             = _.filter(ratedBanners, {BannerType: 'poster'});
			var postersByPopularity = _.reverse(_.sortBy(posters, ['Rating']));
			var popularPoster       = _.head(postersByPopularity);
			request('http://thetvdb.com/banners/' + popularPoster.BannerPath)
				.pipe(fs.createWriteStream('data/' + savedData._id + '_poster.jpg'))
				.on('close', function () {
						finishCallback();
					}
				);
		});
	})
};
var readDirRecursive = function (dir) {
	var dirs  = [],
	    files = fs.readdirSync(dir);
	files.forEach(function (file) {
		if (fs.statSync(path.join(dir, file)).isDirectory()) {
			dirs = dirs.concat(readDirRecursive(path.join(dir, file)));
		} else {
			dirs.push(path.join(dir, file));
		}
	});
	return dirs;
};
var readDir          = function (dir) {
	var dirs  = [],
	    files = fs.readdirSync(dir);
	files.forEach(function (file) {
		if (!fs.statSync(path.join(dir, file)).isDirectory()) {
			dirs.push(path.join(dir, file));
		}
	});
	return dirs;
};
var refresh          = function (directory, refreshCallback) {
	var files = [];
	if (directory.include) {
		files = readDirRecursive(directory.path);
	} else {
		files = readDir(directory.path);
	}
	async.each(files, function (file, callbackFile) {
		if (_.includes(validExtentions, path.extname(file))) {
			var parsedData = parseName(path.basename(file));
			var type       = parsedData.season ? 'tv' : 'movie';
			var data       = {};
			if (type == 'movie') {
				data = {
					title: parsedData.title,
					year: parsedData.year || '',
					file: file
				};
				db.movies.findOne({title: data.title}, function (err, row) {
					if (row == null) {
						db.movies.insert(data, function (err, savedData) {
							updateMovieData(savedData, function () {
								callbackFile(null);
							});
						});
					} else {
						updateMovieData(row, function () {
							callbackFile(null);
						});
					}
				});
			} else if (type == 'tv') {
				data = {
					title: parsedData.title,
					season: parsedData.season,
					episode: parsedData.episode,
					file: file
				};
				db.series.findOne({title: data.title}, function (err, row) {
					if (row == null) {
						db.series.insert(data, function (err, savedData) {
							updateSerieData(savedData, function () {
								callbackFile(null);
							});
						});
					} else {
						updateSerieData(row, function () {
							callbackFile(null);
						});
					}
				});
			} else {
				db.movies.findOne({
					title: data.title,
					season: data.season,
					episode: parsedData.episode
				}, function (err, row) {
					if (row == null) {
						db.movies.insert(data, function (err, savedData) {
							updateData(savedData, function () {
								callbackFile(null);
							});
						});
					} else {
						updateData(row, function () {
							callbackFile(null);
						});
					}
				});
			}
		} else {
			callbackFile(null);
		}
		/*guessit.parseName(parsedData.title).then(function (data) {
		 console.log(data);
		 data.type = data.type == 'episode' ? 'tv' : data.type;
		 if (data.type == 'movie' || data.type == 'tv') {
		 data.file = file;
		 db.movies.findOne({title: data.title}, function (err, row) {
		 if (row == null) {
		 db.movies.insert(data, function (err, savedData) {
		 updateData(savedData, function () {
		 callbackFile(null);
		 });
		 });
		 } else {
		 updateData(row, function () {
		 callbackFile(null);
		 });
		 }
		 });
		 } else {
		 callbackFile(null);
		 }
		 });*/
	}, function () {
		refreshCallback();
	});
};

module.exports = refresh;