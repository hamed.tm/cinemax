var express    = require('express');
var stylus     = require('stylus');
var nib        = require('nib');
var path       = require('path');
var bodyParser = require('body-parser');

//routes
var routes    = require('./routes/main');
var settings  = require('./routes/settings');
var dashboard = require('./routes/dashboard');
var movies    = require('./routes/movies');

var app = express();

app.use(bodyParser.urlencoded({extended: false}));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//stylus
var stylusCompile = function (str, path) {
	return stylus(str).set('filename', path).use(nib());
};
app.use(stylus.middleware({
	src: path.join(__dirname, 'styles'),
	dest: path.join(__dirname, 'public', 'styles'),
	debug: false,
	compile: stylusCompile
}));

//public assets
app.use(express.static(path.join(__dirname, 'public')));

//using routes
app.use('/', dashboard);
app.use('/settings', settings);
app.use('/movies', movies);

module.exports = app;